#!/usr/bin/python

import sys
from numpy import *
import matplotlib.pyplot as plt

COLUMNS = {
    'TICK': 0,
    'THROTTLE': 1,
    'TURBO': 2,
    'POSITION': 3,
    'ANGLE': 4,
    'PIECE': 5,
    'INPIECEDISTANCE': 6,
    'RADIUS': 7,
    'CURVE_ANGLE': 8,
    'SPEED': 9,
    'ACC': 10,
    'DELTAANGLE': 11,
    'DELTA2ANGLE': 12,
    'PREDICTED_SPEED': 13,
    'PREDICTED_ANGLE': 14,
    'PREDICTED_DELTAANGLE': 15,
    'PREDICTED_DELTA2ANGLE': 16,
    # computed values
    'COMPUTED_SPEED': 17,
    'COMPUTED_ACC': 18,
    'COMPUTED_DELTAANGLE': 19,
    'COMPUTED_DELTA2ANGLE': 20,
    'CENTRIPETAL': 21
}

def load_data(filename):
    data = loadtxt(open(filename), delimiter=' ')
    v = hstack((0, data[:, COLUMNS['POSITION']][1:] -
                data[:, COLUMNS['POSITION']][:-1]))[:, newaxis]
    a = hstack((0, 0, data[:, COLUMNS['POSITION']][:-2] -
                2*data[:, COLUMNS['POSITION']][1:-1] +
                data[:, COLUMNS['POSITION']][2:]))[:, newaxis]
    dangle = hstack((0, data[:, COLUMNS['ANGLE']][1:] -
                     data[:, COLUMNS['ANGLE']][:-1]))[:, newaxis]
    d2angle = hstack((0, 0, data[:, COLUMNS['ANGLE']][:-2] -
                      2*data[:, COLUMNS['ANGLE']][1:-1] +
                      data[:, COLUMNS['ANGLE']][2:]))[:, newaxis]
    v1 = v.copy()
    r1 = data[:, COLUMNS['RADIUS']][:, newaxis]
    v1[r1==0] = 0
    curve_sign = sign(data[:, COLUMNS['CURVE_ANGLE']])[:, newaxis]
    r1 = r1*curve_sign
    r1[r1==0] = 1
    centripetal = v1**2/r1
    return hstack((data, v, a, dangle, d2angle, centripetal))

def main():
    filename = sys.argv[1]
    data = load_data(filename)
    offset = 10

    plt.subplot(3, 2, 1)
    plt.plot(data[:, COLUMNS['SPEED']])
    plt.ylabel('speed')

    plt.subplot(3, 2, 2)
    plt.plot(data[:, COLUMNS['COMPUTED_ACC']])
    plt.ylabel('acc')

    plt.subplot(3, 2, 3)
    plt.plot(data[:, COLUMNS['THROTTLE']])
    plt.ylabel('throttle')

    plt.subplot(3, 2, 4)
    plt.plot(data[:, COLUMNS['ANGLE']]*180./pi)
    plt.plot(data[:, COLUMNS['CURVE_ANGLE']]*180./pi, 'c')
    plt.plot(data[:, COLUMNS['RADIUS']]/4, 'g')
    plt.ylabel('slip angle')

    plt.subplot(3, 2, 5)
    plt.plot(data[:, COLUMNS['DELTAANGLE']]*180./pi)
    plt.plot(data[:, COLUMNS['CURVE_ANGLE']]*180./pi/10, 'c')
    plt.plot(data[:, COLUMNS['RADIUS']]/40, 'g')
    plt.ylabel('d/dt slip angle')

    plt.subplot(3, 2, 6)
    plt.plot(data[:, COLUMNS['DELTA2ANGLE']]*180./pi)
    plt.plot(data[:, COLUMNS['CURVE_ANGLE']]*180./pi/100, 'c')
    plt.plot(data[:, COLUMNS['RADIUS']]/400, 'g')
    plt.plot(data[:, COLUMNS['CENTRIPETAL']], 'r')
    plt.ylabel('d^2/dt^2 slip angle')

    plt.figure()
    plt.subplot(2, 1, 1)
    plt.plot(data[:, COLUMNS['SPEED']], 'b')
    plt.plot(hstack((zeros(offset), data[:, COLUMNS['PREDICTED_SPEED']])), 'r')
    plt.ylabel('speed')
    plt.legend(['observed', 'predicted'])

    plt.subplot(2, 1, 2)
    plt.plot(data[:, COLUMNS['ACC']], 'b')
    plt.plot(data[:, COLUMNS['COMPUTED_ACC']], 'b')
    plt.ylabel('acceleration')
    plt.legend(['observed', 'computed'])

    plt.figure()
    plt.subplot(2, 1, 1)
    plt.plot(data[:, COLUMNS['DELTAANGLE']]*180./pi, 'b')
    plt.plot(hstack((zeros(offset), data[:, COLUMNS['PREDICTED_DELTAANGLE']]*180./pi)), 'r')
    plt.ylabel('d/dt slip angle')
    plt.legend(['observed', 'predicted'])

    plt.subplot(2, 1, 2)
    plt.plot(data[:, COLUMNS['DELTA2ANGLE']]*180./pi, 'b')
    plt.plot(hstack((zeros(offset), data[:, COLUMNS['PREDICTED_DELTA2ANGLE']]*180./pi)), 'r')
    plt.plot(data[:, COLUMNS['CENTRIPETAL']], 'c')
    plt.ylabel('d^2/dt^2 slip angle')
    plt.legend(['observed', 'predicted'])

    plt.show()

if __name__ == '__main__':
    main()
