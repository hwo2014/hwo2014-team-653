#!/usr/bin/python

import sys
from visualize import load_data, COLUMNS
from numpy import *
from sklearn import linear_model
import matplotlib.pyplot as plt

def main():
    filenames = sys.argv[1:]
    datasets = []
    for f in filenames:
        datasets.append(load_data(f))
    data = vstack(datasets)

    print
    print 'acceleration model'
    fit_acceleration_model(data)

    print
    print 'low speed model'
    fit_low_speed_model(data, 0., 0.35)

    # print
    # print 'high speed model'
    # fit_high_speed_model(data)

    #fit_combined_model(data)

    fit_acceleration_model(data)



def fit_high_speed_model(data):
    #speed = data[:, COLUMNS['SPEED']]
    angle = data[:, COLUMNS['ANGLE']]
    deltaangle = data[:, COLUMNS['DELTAANGLE']]
    #radius = data[:, COLUMNS['RADIUS']]
    #acc = data[:, COLUMNS['ACC']]
    centripetal = data[:, COLUMNS['CENTRIPETAL']]

    piecedist = data[:, COLUMNS['INPIECEDISTANCE']]


    features = (vstack((centripetal, angle, deltaangle)).T)


    #y = data[:, COLUMNS['DELTAANGLE']][1:] # shift by one
    #y = data[:, COLUMNS['DELTA2ANGLE']][1:] # shift by one
    #y = data[:, COLUMNS['DELTAANGLE']][1:]
    #y = hstack((data[:, COLUMNS['COMPUTED_DELTA2ANGLE']][1:], 0))
    y = hstack((data[:, COLUMNS['DELTA2ANGLE']][1:], 0))

    s = abs(centripetal) > 0.35
    inverted_select = []
    for i in xrange(features.shape[0]):
        if s[i]:
            inverted_select.append(i)
    features = features[s, :]
    y = y[s]

    (coeffs, prediction) = regress(features, y)

def fit_low_speed_model(data, cutoff1, cutoff2):
    centripetal = data[:, COLUMNS['CENTRIPETAL']]
    radius = data[:, COLUMNS['RADIUS']]

    features = (vstack((
                data[:, COLUMNS['ANGLE']],
                data[:, COLUMNS['DELTAANGLE']]
                )).T)
    y = hstack((data[:, COLUMNS['DELTA2ANGLE']][1:], 0))

    s = (radius != 0) & (abs(centripetal) >= cutoff1) & (abs(centripetal) < cutoff2)
    features = features[s, :]
    y = y[s]
    (coeffs, prediction) = regress(features, y)

def fit_combined_model(data):
    centripetal = data[:, COLUMNS['CENTRIPETAL']]
    radius = data[:, COLUMNS['RADIUS']]

    f1 = centripetal.copy()
    f1[f1 < 0.35] = 0
    #f1 = f1 - mean(f1)
    
    features = (vstack((
                f1,
                data[:, COLUMNS['ANGLE']],
                data[:, COLUMNS['DELTAANGLE']]
                )).T)
    y = hstack((data[:, COLUMNS['DELTA2ANGLE']][1:], 0))

    #s = radius != 0
    s = centripetal != 0
    features = features[s, :]
    y = y[s]
    (coeffs, prediction) = regress(features, y)

def fit_acceleration_model(data):
    speed = data[:, COLUMNS['SPEED']]
    features = vstack((
        data[:, COLUMNS['THROTTLE']],
        speed
    )).T
    #y = data[:, COLUMNS['COMPUTED_ACC']]
    y = hstack((data[:, COLUMNS['COMPUTED_ACC']][1:], 0))

    s = (speed > 0) & (speed < 1.0)
    features = features[s, :]
    y = y[s]

    (coeffs, prediction) = regress(features, y, False)

def regress(X, y, fit_intercept=True):
    print 'feature summary'
    print 'shape:', X.shape
    print 'min:', X.min(0)
    print 'mean:', X.mean(0)
    print 'max:', X.max(0)
    print 'std:', std(X, 0)
    
    reg = linear_model.LinearRegression(fit_intercept=fit_intercept)
    reg.fit(X, y)

    print 'intercept:', reg.intercept_
    print 'coefficients:', reg.coef_

    prediction = reg.predict(X)
    residuals = prediction - y

    print 'squared sum', mean(residuals**2)

    plt.subplot(2, 1, 1)
    plt.plot(vstack((y, prediction)).T)
    plt.ylabel('y')

    plt.subplot(2, 1, 2)
    plt.plot(residuals)
    plt.ylabel('residual')

    plt.show()

    return (reg.coef_, prediction)

def centripetal_test():
    filename = sys.argv[1]
    data = load_data(filename)


    X = data[data[:, COLUMNS['CENTRIPETAL']] > 0.35, :]

    y = (X[:, COLUMNS['CENTRIPETAL']] - X[:, COLUMNS['DELTA2ANGLE']])/X[:, COLUMNS['ANGLE']]
    plt.plot(y)
    plt.show()

def acc_test():
    filename = sys.argv[1]
    data = load_data(filename)

    speed = data[:, COLUMNS['SPEED']]
    
    X = vstack((
        data[:, COLUMNS['THROTTLE']],
        speed
    )).T
    #y = hstack((data[:, COLUMNS['COMPUTED_ACC']][1:], 0))
    y = hstack((data[:, COLUMNS['COMPUTED_ACC']]))

    s = (speed > 0.0) & (speed < 1.0)
    #s = range(10)

    X = X[s, :]
    y = y[s]
    print X
    print y

    #for i in xrange(10):
    #    print X[i, :], y[i]
    #print X[:, :10]
    #print y[:10]

    
    plt.plot(X, y, 'o')
    plt.show()
    

if __name__ == '__main__':
    main()
    #centripetal_test()
    #acc_test()
