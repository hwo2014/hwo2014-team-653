import pieces
from car_position import CarPosition

class Track:

    def __init__(self, data):
        self.data = data['track']
        self.laps = data['raceSession'].get('laps', -1)
        # Saver might be ordering the list
        self.lanes = map(pieces.Lane, reversed(self.data['lanes']))
        car_count = len(data['cars'])
        self.pieces = map((lambda d: pieces.create(d, self.lanes, car_count)), self.data['pieces'])
        self.straight_lengths = self.compute_straight_lengths()
        self.cars = [CarPosition()] * car_count

    def compute_straight_lengths(self):
        res = []
        for i in xrange(len(self.pieces)):
            length = 0
            j = i
            while not self.pieces[j % len(self.pieces)].is_curve():
                length += self.pieces[j % len(self.pieces)].lengths[0]
                j += 1
            res.append(length)
        return res

    def valid_lane(self, lane):
        return lane >= 0 and lane < len(self.lanes)

    def distance_between(self, p1, p2, next_lane, switch_to_change):
        if p1.piece == p2.piece:
            return p2.distance - p1.distance
        elif p2.piece < p1.piece:
            p1_left = self.pieces[p1.piece].distance_left(p1, next_lane)
            middle_length = 0
            for i in xrange(p1.piece, len(self.pieces)-1):
                middle_length += self.piece_length(i, p1.lane)
            for i in xrange(0, p2.piece-1):
                middle_length += self.piece_length(i, p1.lane)
            return p1_left + middle_length + p2.distance
        else:
            p1_left = self.pieces[p1.piece].distance_left(p1, next_lane)
            middle_length = self.distance((lambda i: i == p2.piece), p1, 1, next_lane, switch_to_change)
            return p1_left + middle_length + p2.distance

    def distance_to_start_edge(self, p1, target_piece):
        p2 = Position()
        p2.piece = target_piece
        return self.distance_between(p1, p2)

    def distance_to_curve(self, position):
        piece_left = self.pieces[position.piece].distance_left(position)
        middle_pieces = self.distance((lambda i: self.pieces[i].curve), position)
        return  piece_left + middle_pieces

    def distance_to_curve(self, position, next_lane, switch_to_change):
        return self.distance_to_condition(position, (lambda i: self.pieces[i].curve), 1, next_lane, switch_to_change)

    def distance_to_switch(self, position, next_lane, switch_to_change):
        return self.distance_to_condition(position, (lambda i: self.pieces[i].switch), 1, next_lane, switch_to_change)

    def distance_to_third_switch(self, position, next_lane, switch_to_change):
        return self.distance_to_condition(position, (lambda i: self.pieces[i].switch), 3, next_lane, switch_to_change)

    def distance_to_condition(self, position, condition, condition_count, next_lane, switch_to_change):
        piece_left = self.pieces[position.piece].distance_left(position, next_lane)
        middle_pieces = self.distance(condition, position, condition_count, next_lane, switch_to_change)
        return piece_left + middle_pieces

    def distance(self, condition, position, condition_count, next_lane, count_to_switch):
        def piece_distance(i, count_to_switch):
            if self.pieces[i].switch:
                if count_to_switch == 1:
                    return self.pieces[i].switch_length(position.lane, next_lane), count_to_switch
                else:
                    count_to_switch -= 1
                    return self.pieces[i].lengths[position.lane], count_to_switch
            else:
                return self.pieces[i].lengths[position.lane], count_to_switch

        count = 0
        distance = 0
        if condition(position.piece):
            count += 1
            if count == condition_count:
                return distance
        for i in range(position.piece + 1, len(self.pieces)):
            if condition(i):
                count += 1
                if count == condition_count:
                    return distance
            else:
                (d, count_to_switch) = piece_distance(i, count_to_switch)
                distance += d
        return distance + self.distance(condition, position.next_lap(), condition_count, next_lane, count_to_switch)

    def piece_length(self, piece_index, lane):
        return self.pieces[piece_index].lengths[lane]

    def curve_radius(self, piece_index, lane):
        piece = self.pieces[piece_index % len(self.pieces)]
        if piece.is_curve():
            curvesign = -1 if piece.a < 0 else +1
            return piece.r + curvesign*piece.lane_distances[lane]
        else:
            return 0.0

    def curve_angle(self, piece_index):
        piece = self.pieces[piece_index % len(self.pieces)]
        if piece.is_curve():
            return piece.a
        else:
            return 0.0

    def next_piece(self, start_piece, start_distance, start_lane, delta):
        assert delta >= 0
        distance = start_distance + delta
        piece_length = self.piece_length(start_piece, start_lane)
        updated_piece = start_piece
        lap_delta = 0
        while distance > piece_length:
            updated_piece += 1
            if updated_piece >= len(self.pieces):
                updated_piece = 0
                lap_delta += 1
            distance -= piece_length
            piece_length = self.piece_length(updated_piece, start_lane)
        return (updated_piece, distance, lap_delta)

    def is_curve_start(self, piece_index):
        ca1 = self.curve_angle((piece_index-1) % len(self.pieces))
        ca2 = self.curve_angle(piece_index % len(self.pieces))
        straight_followed_by_curve = (ca1 == 0) and (ca2 != 0)
        different_curve_direction = ca1*ca2 < 0
        return straight_followed_by_curve or different_curve_direction

    def find_last_curve_piece(self, curve_start_index):
        i = curve_start_index % len(self.pieces)
        # ca = self.curve_angle(i)
        # if ca == 0:
        #     return i
        # else:
        #     i = (i+1) % len(self.pieces)
        #     while ca*self.curve_angle(i) > 0:
        #         i = (i+1) % len(self.pieces)
        #     return (i-1) % len(self.pieces)

        if self.curve_angle(i) == 0:
            return i
        
        while self.curve_angle(i) != 0:
            i = (i + 1) % len(self.pieces)
        return (i-1) % len(self.pieces)

    def is_curve(self, piece_index):
        return self.curve_angle(piece_index) != 0

    def min_radius_for_curve(self, curve_start_index, lane):
        min_radius = 1000
        last = self.find_last_curve_piece(curve_start_index)
        i = curve_start_index
        while i <= last:
            r = self.curve_radius(i, lane)
            if r < min_radius:
                min_radius = r
            i = (i+1) % len(self.pieces)
        return min_radius

    def next_curve_start(self, from_piece):
        i = from_piece % len(self.pieces)
        start_angle = self.curve_angle(i)
        i += 1
        while self.curve_angle(i)*start_angle > 0:
            i = (i + 1) % len(self.pieces)
        while self.curve_angle(i) == 0:
            i = (i + 1) % len(self.pieces)
        return i

    def nearest_car(self, position):
        to_beat = None
        to_beat_index = -1
        for index, car in enumerate(self.cars):
            if car.is_ahead(position) and (to_beat is None or not car.is_ahead(to_beat)):
                to_beat = car
                to_beat_index = index
        return to_beat_index
