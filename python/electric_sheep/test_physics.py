import unittest
import math
import physics.curve

class TestCurveMath(unittest.TestCase):

    def test_curve_length(self):
        r = 10
        a = math.pi 
        from_center = 0
        self.assertEqual(physics.curve.length(r, a, from_center), math.pi * 10)
        self.assertEqual(physics.curve.length(3, a, from_center), math.pi * 3)
        self.assertEqual(physics.curve.length(r, 1, from_center), 10)
        self.assertEqual(physics.curve.length(r, -a, from_center), math.pi * 10)
        self.assertEqual(physics.curve.length(r, a, 1), math.pi * 11)
        self.assertEqual(physics.curve.length(r, a, -1), math.pi * 9)
        self.assertEqual(physics.curve.length(r, -a, -1), math.pi * 11)

if __name__ == '__main__':
    unittest.main()
