from numpy import *
from numpy.linalg import lstsq

class CurveModel:
    """Learn and predict slip angle dynamics."""
    
    INTERCEPT = 0
    CENTRIPETAL = 1
    SLIPANGLE = 2
    ANGLESPEED = 3
    
    def __init__(self):
        self.features = []
        self.y = []
        self.coeffs_low = zeros(4)
        self.coeffs_high = zeros(4)
        self.centripetal_cutoff = 0.35 # fixme: learn
        self.max_slip_angle = 90./180.*pi
        self.learned_max_slip_angle = False
        self.max_speed_factor = 0.674

    def collect_data(self, speed, radius, curve_angle, slip_angle, dangle, d2angle):
        if radius != 0:
            self.features.append(self._compute_features(speed, radius, curve_angle,
                                                        slip_angle, dangle))
            self.y.append(d2angle)

    def fit(self):
        if not self.features:
            print 'Error learning the curve model: No data'
            return

        self.fit_max_slip_angle()
        self.fit_model()

    def fit_model(self):
        X = array(self.features)[:-1, :]
        y = array(self.y)[1:] # shift by one

        self.coeffs_low, res1, nsamples_low = \
          self.fit_low_speed_model(X, y, self.centripetal_cutoff)
        self.coeffs_high, res2, nsamples_high = \
          self.fit_high_speed_model(X, y, self.centripetal_cutoff)
        
        print 'Fitted curve model'
        print 'low speed: %s using %d samples' % (self.coeffs_low, nsamples_low)
        print 'high speed: %s using %d samples' % (self.coeffs_high, nsamples_high)
        print 'centripetal cutoff:', self.centripetal_cutoff
        print 'squared residual sum', sum(res1**2) + sum(res2**2)

    def fit_high_speed_model(self, X, y, cutoff):
        selected = abs(X[:, self.CENTRIPETAL]) >= cutoff
        X_subset = X[selected, :]
        y_subset = y[selected]
        return self.fit_regression(X_subset, y_subset)

    def fit_low_speed_model(self, X, y, cutoff):
        cp = abs(X[:, self.CENTRIPETAL])
        selected = (cp != 0) & (cp < cutoff)
        X_subset = X[selected, :]
        X_subset = X_subset[:, [self.INTERCEPT, self.SLIPANGLE, self.ANGLESPEED]]
        y_subset = y[selected]
        res = self.fit_regression(X_subset, y_subset)
        return (array([res[0][0], 0.0, res[0][1], res[0][2]]), res[1], res[2])

    def fit_regression(self, X, y):
        num_samples = len(y)
        if num_samples > 3:
            coeffs, residuals = lstsq(X, y)[:2]
        else:
            print 'Error learning the curve model: Too few samples'
            coeffs = zeros(X.shape[1])
            residuals = zeros(0)
        return (coeffs, residuals, num_samples)

    def fit_max_slip_angle(self, observed_angle=None):
        new_value = 0.0
        if self.features:
            X = array(self.features)
            new_value = max(abs(X[:, self.SLIPANGLE]).max(), new_value)
        if observed_angle is not None:
            new_value = max(abs(observed_angle), new_value)
            
        if self.learned_max_slip_angle:
            self.max_slip_angle = max(new_value, self.max_slip_angle)
        else:
            self.max_slip_angle = new_value
            self.learned_max_slip_angle = True
        
        print 'Learned max slip angle: %f degrees' % (self.max_slip_angle*180./pi)
        
    def predict_one_step(self, speed, radius, curve_angle, slip_angle, dangle):
        """Returns prediction of the angle variables at the next time step."""
        x = self._compute_features(speed, radius, curve_angle, slip_angle, dangle)
        if abs(x[self.CENTRIPETAL]) >= self.centripetal_cutoff:
            pred_d2angle = dot(self.coeffs_high, x)
        else:
            pred_d2angle = dot(self.coeffs_low, x)
        return (slip_angle + dangle + pred_d2angle, dangle + pred_d2angle, pred_d2angle)

    def predict_angle_acceleration(self, speed, radius, curve_angle, slip_angle, dangle):
        return self.predict_one_step(speed, radius, curve_angle, slip_angle, dangle)[2]

    def max_speed_for_curve(self, radius):
        return min(self.max_speed_factor*sqrt(radius), 10.)

    def decrease_max_curve_speed(self):
        self.max_speed_factor = 0.90*self.max_speed_factor

    def _compute_features(self, speed, radius, curve_angle, slip_angle, dangle):
        if radius != 0:
            centripetal = sign(curve_angle)*speed**2/radius
        else:
            centripetal = 0.0
        return array([1, centripetal, slip_angle, dangle])
