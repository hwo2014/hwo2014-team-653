import physics.curve
import core
import math

class Piece(object):

    def __init__(self, piece_data, lanes, car_count):
        self.curve = is_curve(piece_data)
        self.lanes = lanes
        self.switch = 'switch' in piece_data
        self.lengths = []
        self.drivers = [Driver()] * car_count

    def distance_left(self, position, next_lane):
        if self.switch and next_lane is not None:
            return self.switch_length(position.lane, next_lane) - position.distance
        else:
            return self.lengths[position.lane] - position.distance

    def is_curve(self):
        return False

class Straight(Piece):

    def __init__(self, data, lanes, car_count):
        super(Straight, self).__init__(data, lanes, car_count)
        self.lengths = [data['length']] * len(lanes)

    def switch_length(self, lane, next):
        d = 23.2384854622
        change_distance = math.fabs(self.lanes[lane].from_center - self.lanes[next].from_center)
        return self.lengths[lane] - d + math.sqrt(d ** 2 + change_distance ** 2)

class Curve(Piece):

    def __init__(self, data, lanes, car_count):
        super(Curve, self).__init__(data, lanes, car_count)
        self.r = data['radius']
        self.lane_distances = [x.from_center for x in lanes]
        self.a = core.radians(data['angle'])
        self.lengths = map((lambda l: physics.curve.length(self.r, self.a, l.from_center)), lanes)

    def is_curve(self):
        return True

    def switch_length(self, lane, next):
        change_distance = math.fabs(self.lanes[lane].from_center - self.lanes[next].from_center)
        r = self.r + self.lanes[lane].from_center
        d = 25.0
        alpha = d / r
        # Fishy
        r2 = r + change_distance
        y = r2 - r * math.cos(alpha)
        x = r * math.sin(alpha)
        average_length = (self.lengths[lane] + self.lengths[next]) / 2
        if self.lengths[lane] > self.lengths[next]:
            avoided_d = d
        else:
            avoided_d = r2 * alpha
        return average_length + math.sqrt(x ** 2 + y ** 2) - avoided_d


class Lane:

    def __init__(self, data):
        self.index = data['index']
        self.from_center = data['distanceFromCenter']


def create(data, lanes, car_count):
    if(is_curve(data)):
        return Curve(data, lanes, car_count)
    else:
        return Straight(data, lanes, car_count)
    
def is_curve(data):
    return 'radius' in data


class Driver:
    def __init__(self):
        self.last_distance = 0
        self.speed = 0
        self.lane = 0

    def update(self, distance, lane):
        self.lane = lane
        if distance > self.last_distance:
            self.speed = distance - self.last_distance
            self.last_distance = distance
