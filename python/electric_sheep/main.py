import json
import math
import socket
import sys
from car import Car
from core import AI
from telemetry import Telemetry

class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.gameTick = -1
        self.telemetry = Telemetry('--telemetry' in sys.argv)

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def msg_with_gametick(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data, "gameTick": self.gameTick}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def create_race(self, track):
        return self.msg("joinRace", {
                "botId": {
                    "name": self.name,
                    "key": self.key
                },
                "trackName": track,
                "carCount": 2
             })

    def throttle(self, throttle):
        self.msg_with_gametick("throttle", throttle)

    def turbo(self):
        self.msg_with_gametick("turbo", "Futum!")

    def switch(self, direction):
        self.ourCar.switching = True
        if direction == 'Right':
            self.ourCar.next_lane = self.ourCar.position.lane + 1
        if direction == 'Left':
            self.ourCar.next_lane = self.ourCar.position.lane - 1
        self.msg("switchLane", direction)

    def ping(self):
        self.msg_with_gametick("ping", {})

    def run(self, track):
        if track:
            self.create_race(track)
        else:
            self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def update_drivers(self, data):
        for index, car in enumerate(data):
            track = self.ourCar.track
            pos_data = car['piecePosition']
            pind = pos_data['pieceIndex']
            piece = track.pieces[pind]
            lane = pos_data['lane']['endLaneIndex']
            distance = pos_data['inPieceDistance']
            driver = piece.drivers[index]
            driver.update(distance, lane)
            track.cars[index].update(pind, distance, lane)


    def on_car_positions(self, data):
        self.update_drivers(data)
        our_data = filter((lambda p: p['id'] == self.ourCar.car_id), data)[0]
        self.ourCar.update(our_data)

        if self.ourCar.can_switch and self.ourCar.speed > 0:
            self.ourCar.can_switch = False
            direction = self.ai.switch_direction()
            if direction is not None:
                self.switch(direction)

        if self.ai.should_activate_turbo():
            self.turbo()
        else:
            self.throttle(self.ai.adjust_throttle(self.gameTick))

        self.ai.collect_learning_data(self.gameTick)
        if self.telemetry.enabled:
            self.take_telemetry_sample()

    def take_telemetry_sample(self):
        state = self.ourCar.game_state(self.gameTick)
        t = self.ai.throttle
        pred = self.ai.predict_n_steps(state, 10, t, self.ourCar.track)[-1]
        self.telemetry.log(self.gameTick, t, self.ourCar.physics.turbo_factor,
                           self.ourCar.travelled,
                           self.ourCar.position.angle, self.ourCar.position.piece,
                           self.ourCar.position.distance,
                           self.ourCar.track.curve_radius(self.ourCar.position.piece,
                                                          self.ourCar.position.lane),
                           self.ourCar.track.curve_angle(self.ourCar.position.piece),
                           self.ourCar.speed, self.ourCar.acc,
                           self.ourCar.angle_speed, self.ourCar.angle_acc,
                           pred.speed, pred.angle(), pred.angle_speed,
                           pred.angle_acc)

    def on_crash(self, data):
        if data.get('name', '') == self.name:
            print('We crashed!')
            self.ai.on_crash(self.ourCar.previous_angle())
        else:
            print("Someone crashed")

    def on_lap_finished(self, data):
        print("Lap finished")
        self.ai.on_lap_finished()

    def on_game_end(self, data):
        print("Race ended")
        self.ourCar.dump()
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))

    def on_your_car(self, car_id):
        print("Our name: ", car_id['name'])
        print("Our color: ", car_id['color'])
        self.ourCar = Car(car_id)
        self.ai = AI(self.ourCar)

    def on_init(self, data):
        self.ourCar.init_track(data['race'])
        our_data = filter((lambda p: p['id'] == self.ourCar.car_id), data['race']['cars'])[0]
        self.ourCar.init_geometry(our_data)
        self.ai.config_session(data['race']['raceSession'])

    def on_turbo_available(self, data):
        print 'turbo available on tick %d' % self.gameTick
        self.ourCar.on_turbo_available(data)

    def on_turbo_start(self, data):
        if data.get('name', '') == self.name:
            print 'Turbo activated'
            self.ourCar.start_turbo()

    def on_turbo_end(self, data):
        if data.get('name', '') == self.name:
            print 'Turbo over'
            self.ourCar.end_turbo()
        
    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'lapFinished': self.on_lap_finished,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'yourCar': self.on_your_car,
            'gameInit': self.on_init,
            'turboAvailable': self.on_turbo_available,
            'turboStart': self.on_turbo_start,
            'turboEnd': self.on_turbo_end
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if 'gameTick' in msg:
                self.gameTick = msg['gameTick']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            if 'gameTick' in msg and msg['gameTick'] % 60 == 0:
                print("Speed ", self.ourCar.speed)
            line = socket_file.readline()

if __name__ == "__main__":
    if len(sys.argv) < 5:
        print("Usage: ./run host port botname botkey [track]")
        print("Tracks: keimola, germany, usa")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        if len(sys.argv) > 5:
            track = sys.argv[5]
        else:
            track = None
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run(track)
