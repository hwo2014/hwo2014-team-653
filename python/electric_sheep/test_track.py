import unittest
import math
from track import Track


class PositionMuck:

    def __init__(self, piece, x, lane = 0):
        self.piece = piece
        self.distance = x
        self.lane = lane

    def next_lap(self):
        return PositionMuck(0, 0, self.lane)

class TestTrack(unittest.TestCase):

    def setUp(self):
        data = {
                "id": "indianapolis",
                "name": "Indianapolis",
                "pieces": [
                    {
                        "length": 100.0
                        },
                    {
                        "length": 100.0,
                        "switch": "true"
                        },
                    {
                        "radius": 200,
                        "angle": 22.5
                        },
                    {
                        "radius": 200,
                        "angle": 22.5
                        }
                    ],
                "lanes": [
                    {
                        "distanceFromCenter": -20,
                        "index": 0
                        },
                    {
                        "distanceFromCenter": 0,
                        "index": 1
                        },
                    {
                        "distanceFromCenter": 20,
                        "index": 2
                        }
                    ],
                "startingPoint": {
                    "position": {
                        "x": -340.0,
                        "y": -96.0
                        },
                    "angle": 90.0
                    }
                }
        self.track = Track(data)

    def test_distance_between(self):
        distance = self.track.distance_between(PositionMuck(0, 0), PositionMuck(0, 1))
        self.assertEqual(distance, 1)
        distance = self.track.distance_between(PositionMuck(2, 0), PositionMuck(2, 4))
        self.assertEqual(distance, 4)
        distance = self.track.distance_between(PositionMuck(1, 0), PositionMuck(2, 4))
        self.assertEqual(distance, 104)
        distance = self.track.distance_between(PositionMuck(1, 4), PositionMuck(2, 4))
        self.assertEqual(distance, 100)
        distance = self.track.distance_between(PositionMuck(2, 0, 1), PositionMuck(3, 1, 1))
        curve_distance = math.pi * 200 * 22.5 / 180
        self.assertEqual(distance, curve_distance + 1)
        distance = self.track.distance_between(PositionMuck(2, 5, 1), PositionMuck(3, 1, 1))
        curve_distance = math.pi * 200 * 22.5 / 180
        self.assertEqual(distance, curve_distance - 5 + 1)
        distance = self.track.distance_between(PositionMuck(2, 5, 2), PositionMuck(3, 1, 2))
        curve_distance = math.pi * 180 * 22.5 / 180
        self.assertEqual(distance, curve_distance - 5 + 1)
        distance = self.track.distance_between(PositionMuck(3, 5, 1), PositionMuck(0, 1, 1))
        curve_distance = math.pi * 200 * 22.5 / 180
        self.assertEqual(distance, curve_distance - 5 + 1)

if __name__ == '__main__':
    unittest.main()
