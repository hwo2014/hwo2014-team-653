import math

def length(radius, angle, from_center):
    if(angle > 0):
        r = radius + from_center
    else:
        r = radius - from_center
    return r * math.fabs(angle)
