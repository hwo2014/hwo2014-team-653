import math
from numpy import array
from numpy.linalg import lstsq

class Physics:

    def __init__(self, mu, power):
        self.mu = mu
        self.T = power
        self.turbo_factor = 1.0
        self.features = []
        self.y = []
        self.learned = False
    
    def throttle_factor(self):
        return self.T * self.turbo_factor
        
    def force(self, throttle):
        return throttle * self.throttle_factor()

    def counter_force(self, v):
        return self.mu * v

    def throttle(self, v, a):
        return (a + self.counter_force(v)) / self.throttle_factor()

    # a = F / m, m = 1
    def a(self, v, throttle):
        return self.force(throttle) - self.counter_force(v)

    # n of geometric progression
    def ticks_to_speed(self, v, v_t):
        if(v > 0 and v > v_t):
            base = 1 + self.mu
            time = math.log(v / v_t, base)
            return math.ceil(time)
        else:
            return 0

    # Geometric serie
    def distance_to_speed(self, v, v_t):
        n = self.ticks_to_speed(v, v_t)
        if(n == 0):
            return 0
        else:
            a = v
            r = 1 / (1 + self.mu)
            return a * (1 - r ** (n + 1)) / (1 - r)

    # v = v0 + at, t = 1
    def next_v(self, v_0, throttle_0):
        a_0 = self.a(v_0, throttle_0)
        v_1 = v_0 + a_0
        return v_1

    def next_x(self, x_0, v_0, throttle_0):
        return x_0 + self.next_v(v_0, throttle_0)

    def power(self, throttle_0, v_0, v_1):
        a = v_1 - v_0
        return (a + self.counter_force(v_0)) / throttle_0

    def enable_turbo(self, factor):
        self.turbo_factor = factor

    def disable_turbo(self):
        self.turbo_factor = 1.0

    def collect_data(self, throttle, speed, acc, radius, tick):
        # tick > 2 to wait until the acceleration estimator has stabilized
        if radius == 0 and speed > 0 and speed < 1 and tick > 2:
            self.features.append(array([throttle, speed]))
            self.y.append(acc)

            if len(self.features) >= 3:
                self.fit()

    def fit(self):
        if self.learned:
            return
        
        if len(self.features) < 3:
            print 'Error learning the acceleration model: No enough data'
            return

        # shift by one
        X = self.features[:-1]
        a = self.y[1:]
        
        self.T = (a[1]*X[0][1] - a[0]*X[1][1]) / (X[0][1]*X[1][0] - X[0][0]*X[1][1])
        self.mu = (self.T*X[0][0] - a[0])/X[0][1]

        res = array([self.T*X[0][0] - self.mu*X[0][1] - a[0],
                     self.T*X[1][0] - self.mu*X[1][1] - a[1]])

        print 'Fitted acceleration model'
        print 'T = %f, mu = %f' % (self.T, self.mu)
        print 'num samples = %d' % (len(X))
        print 'squared residual sum', sum(res**2)

        self.learned = True


# Calculates the radius of the mass center track
# r_c Radius of the curve
# r_a Radius for the car's moment causing the off-track movement
# b   Angle of the car towards the track
#
# Law of cosines
def mass_center_radius(r_c, r_a, b):
    return math.sqrt(r_a ** 2 + r_c ** 2 - 2 * r_a * r_c * math.cos(b + math.pi / 2))

# Calculates the angle of the centrifugal force towards the car
# r_c Radius of the curve
# r_a Radius for the car's moment causing the off-track movement
# r_m Radius of the mass center track
#
# Law of cosines
def centrifugal_angle(r_c, r_a, r_m):
    return math.pi - math.acos((r_a ** 2 - r_c ** 2 + r_m ** 2) / (2 * r_a * r_m))


def centrifugal_force(v, r_m):
    return v ** 2 / r_m

# Calculates the assumed centrifugal counter force
# r_c Radius of the curve
# a   Angle acceleration
# c   The angle of the centrifugal force towards the car
# F_c Centrifugal force
def centrifugal_counter_force(r_c, a, c, F_c):
    return F_c - (a * r_c) / math.sin(c)


