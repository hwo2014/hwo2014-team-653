def depth_first_search(start, target, maxiter=100):
    openlist = []
    came_from = {}
    
    openlist.append(start)
    i = 0
    while openlist and i < maxiter:
        x = openlist.pop()
        i += 1

        if x.is_target(target):
            path = [target]
            
            while came_from.has_key(x):
                x = came_from[x]
                path.insert(0, x)

            return path

        adjacent = x.adjacent()
        for y in adjacent:
            came_from[y] = x
            openlist.append(y)

    return None

class GameStateNode(object):
    def __init__(self, gamestate, throttle, simulator, track, maxspeed, maxangle):
        self.state = gamestate
        self.throttle = throttle
        self.simulator = simulator
        self.track = track
        self.maxspeed = maxspeed
        self.maxangle = maxangle
        self.throttle_levels = [0.0, 0.25, 0.5, 0.75, 1.0]
        self.step_size = 10

    def adjacent(self):
        neighbors = []
        for t in self.throttle_levels:
            states = self.simulator.predict_n_steps(self.state,
                self.step_size, t, self.track)
            crashed = any([abs(s.angle()) >= self.maxangle for s in states])
            if crashed:
                continue
            neighbors.append(GameStateNode(states[-1], t, self.simulator,
                self.track, self.maxspeed, self.maxangle))

        return neighbors
    
    def distance(self, other):
        # Time in ticks. Assumes that other is a neighbor to this node.
        return self.step_size

    def estimated_distance(self, other):
        # estimate time to the start edge of the target piece if we
        # were to drive at the maximum speed
        pos = self.state.position
        d = self.track.distance_to_start_edge(pos, other.piece)
        return d/self.maxspeed

    def is_target(self, target):
        return self.state.position.piece >= target.piece

class TargetNode(object):
    def __init__(self, piece):
        self.piece = piece

    def adjacent(self):
        return []

    def distance(self, other):
        return None

    def estimated_distance(self, other):
        return 0.0
