import math
import curvemodel
from search import *

class Position:

    def __init__(self, data=None):
        if(data is None):
            self.piece = 0
            self.lane = 0
            self.distance = 0
            self.lap = 0
            self.angle = 0
        else:
            self.angle = radians(data['angle'])
            piece = data['piecePosition']
            self.piece = piece['pieceIndex']
            self.lane = piece['lane']['startLaneIndex']
            self.end_lane = piece['lane']['endLaneIndex']
            self.distance = piece['inPieceDistance']
            self.lap = piece['lap']

    def next_lap(self):
        p = Position()
        p.lane = self.lane
        p.lap = self.lap + 1
        return p

    def next(self, delta, delta_angle, track):
        p = Position()
        p.lane = self.lane
        p.angle = self.angle + delta_angle
        next_piece, next_in_piece_distance, lap_delta = \
            track.next_piece(self.piece, self.distance, self.lane, delta)
        p.piece = next_piece
        p.distance = next_in_piece_distance
        p.lap = self.lap + lap_delta
        return p


class AI:
    ANGLE_MARGIN = 0.6

    def __init__(self, car):
        self.car = car
        self.curve_speed = 6.4
        self.learning = True
        self.has_crashed = False
        self.crash_count = 0
        self.curvemodel = curvemodel.CurveModel()
        self.last_curve_start = -1
        self.throttle = 1.0
        self.turbo_was_last_checked_on_piece = -1
        self.turbo_straight_length = -1
        self.curve_speed_computed_on_piece = -1

    def config_session(self, race_session):
        if race_session.get('quickRace', True) == False:
            self.learning = False

    def on_lap_finished(self):
        if self.learning:
            self.learning = False
            self.curvemodel.fit()
            self.car.physics.fit()

    def on_crash(self, previous_angle):
        self.has_crashed = True
        self.crash_count += 1
        self.curvemodel.fit_max_slip_angle(previous_angle)
        if self.crash_count >= 2:
            self.curvemodel.decrease_max_curve_speed()
        self.car.reset_state()
        self.throttle = 1.0
        if self.learning:
            self.curvemodel.fit()
            self.car.physics.fit()

    def switch_direction(self):
        track = self.car.track
        pos = self.car.position
        next_switch = pos.next(0, 0, track)
        current = track.distance_to_third_switch(next_switch, None, 0)
        nearest_car = track.nearest_car(pos)
        piece = track.pieces[pos.piece]
        if nearest_car > -1:
            nearest_car_driver = piece.drivers[nearest_car]
            print "nearest car", nearest_car, "speed", nearest_car_driver.speed
            if nearest_car_driver.speed < self.car.speed and track.cars[nearest_car].lane == pos.lane:
                print "going to crash with other car"
                current += 50
        right = pos.lane + 1
        left = pos.lane - 1
        if track.valid_lane(right):
            next_switch.lane = right
            if current > track.distance_to_third_switch(next_switch, right, 1):
                return "Right"
        if track.valid_lane(left):
            next_switch.lane = left
            if current > track.distance_to_third_switch(next_switch, left, 1):
                return "Left"
        else:
            return None

    def learn(self):
        track = self.car.track
        pind = self.car.position.piece
        self.curvemodel.collect_data(
            self.car.speed,
            track.curve_radius(pind),
            track.curve_angle(pind),
            self.car.position.angle,
            self.car.angle_speed,
            self.car.angle_acc)

    def collect_learning_data(self, tick):
        if self.learning:
            track = self.car.track
            pind = self.car.position.piece
            lane = self.car.position.lane
            radius = track.curve_radius(pind, lane)
            self.curvemodel.collect_data(
                self.car.speed,
                radius,
                track.curve_angle(pind),
                self.car.position.angle,
                self.car.angle_speed,
                self.car.angle_acc)

            self.car.physics.collect_data(self.throttle, self.car.speed,
                                          self.car.acc, radius, tick)
    
    def on_curving(self):
        self.car.curvemeter()
        a_needed = self.curve_speed - self.car.speed
        throttle = self.car.physics.throttle(self.car.speed, a_needed)

        if throttle > 1:
            return 1
        if throttle < 0:
            return 0
        else:
            return throttle

    def predict_n_steps(self, start_state, steps, throttle, track):
        state = start_state
        states = []
        for i in xrange(steps):
            pind = state.position.piece
            lane = state.position.lane
            acc = self.car.physics.a(state.speed, throttle)
            angle_acc = self.curvemodel.predict_angle_acceleration(
                state.speed, track.curve_radius(pind, lane),
                track.curve_angle(pind), state.angle(), state.angle_speed)
            state = state.update(acc, angle_acc, track)
            states.append(state)
        return states

    def adjust_throttle(self, tick):
        pind = self.car.position.piece
        track = self.car.track
        if pind != self.curve_speed_computed_on_piece and not track.pieces[pind].is_curve():
            self.curve_speed_computed_on_piece = pind
            next_curve = track.next_curve_start(pind)
            print 'piece', pind, 'next_curve', next_curve
            lane = self.car.position.end_lane
            min_radius = track.min_radius_for_curve(next_curve, lane)
            max_speed = self.curvemodel.max_speed_for_curve(min_radius)

            print 'curve at', (next_curve), 'computed at', pind,
            print 'min radius', min_radius, 'max speed', max_speed, 'factor', self.curvemodel.max_speed_factor
            self.curve_speed = max_speed

        if pind != self.last_curve_start and track.is_curve_start(pind):
            self.last_curve_start = pind
            print 'curve at', pind, 'curve speed', self.curve_speed
            
        if self.learning and (tick < 100):
            # vary throttle values to learn the physics
            self.throttle = 0.2 + 0.8*(tick % 50)/50
        elif self.learning and not self.has_crashed:
            # cause at least one crash so that we learn the max slip angle
            self.throttle = 1.0
        elif self.car.is_curving():
            self.throttle = self.on_curving()
        elif self.car.is_next_late(self.curve_speed):
            d = self.car.distance_to_late(self.curve_speed)
            self.throttle = self.car.throttle_needed(d)
        else:
            self.throttle = 1.0

        return self.throttle

    def generate_throttle_program(self, tick, target_piece):
        start_state = self.car.game_state(tick)
        node = GameStateNode(start_state, self.throttle, self, self.car.track,
                             10.0, self.curvemodel.max_slip_angle*self.ANGLE_MARGIN)
        target = TargetNode(target_piece % len(self.car.track.pieces))
        path = depth_first_search(node, target)
        if not path:
            # didn't find a path!? Just go really slowly for a while
            # and hope we don't crash
            return [(tick, 0.0), (tick+200, 0.6), (tick+500, 1.0)]
        else:
            return [(n.state.tick, n.throttle) for n in path if hasattr(n, 'throttle')]

    def min_length_for_turbo(self):
        if self.turbo_straight_length == -1:
            self.turbo_straight_length = min(400, max(self.car.track.straight_lengths))
        return self.turbo_straight_length
        
    def should_activate_turbo(self):
        pind = self.car.position.piece
        if (self.car.has_turbo() and
                self.turbo_was_last_checked_on_piece != pind and
                not self.learning):
            self.turbo_was_last_checked_on_piece = pind
            if self.car.track.straight_lengths[pind] >= self.min_length_for_turbo():
                return True

        return False

def radians(degrees):
    return degrees * math.pi / 180

