import unittest
from car import Car

class MuckTrack:
    def __init__(self):
        self.mu = 0.02

class TestCar(unittest.TestCase):

    def setUp(self):
        self.car = Car("asdf")
        self.car.track = MuckTrack()

    def testA(self):
        self.assertEqual(self.car.a(1), 0.2)

    def testBasics(self):
        self.assertEqual(self.car.power, 0.2)

    def testForce(self):
        self.assertEqual(self.car.force(1), 0.2)

if __name__ == '__main__':
    unittest.main()
