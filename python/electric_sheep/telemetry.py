import time
import os
import os.path

class Telemetry:
    def __init__(self, enabled):
        self.enabled = enabled
        if enabled:
            if not os.path.exists('data'):
                os.makedirs('data')
            self.fd = open('data/telemetry-%d' % int(time.time()), 'w')
        else:
            self.fd = None
        self.comment('tick throttle turbo position angle piece in_piece_distance radius curve_angle speed acc angle_speed angle_acc pred_speed pred_angle pred_angle_speed pred_angle_acc')

    def write(self, msg):
        if self.enabled and self.fd:
            self.fd.write(msg)
    
    def log(self, tick, throttle, turbo, position, angle, piece_index,
            in_piece_distance, curve_radius, curve_angle, speed, acc,
            angular_speed, angular_acc, predicted_speed, predicted_angle,
            predicted_angle_speed, predicted_angle_acc):
        self.write('%d %f %f %f %f %d %f %f %f %f %f %f %f %f %f %f %f\n' %
                   (tick, throttle, turbo, position, angle, piece_index,
                    in_piece_distance, curve_radius, curve_angle,
                    speed, acc, angular_speed, angular_acc, predicted_speed,
                    predicted_angle, predicted_angle_speed,
                    predicted_angle_acc))

    def comment(self, msg):
        self.write('# ' + str(msg) + '\n')
        
    def raceStart(self):
        pass

    def crash(self):
        self.comment('crashed!')

    def spawn(self):
        self.comment('spawn')
