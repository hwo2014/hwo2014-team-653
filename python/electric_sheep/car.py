import os
import os.path
import math
import physics.car
from track import Track
from core import Position
from turbo import Turbo
from gamestate import GameState
from numpy import *

class Car:
    def __init__(self, car_id):
        self.car_id = car_id
        self._travelled = zeros(3)
        self.travelled = 0.0
        self.speed = 0
        self.acc = 0
        self._angles = zeros(3)
        self.angle_speed = 0
        self.angle_acc = 0
        self.position = Position()
        self.track = None
        # Moment radius in corners
        self.r_a = None
        self.physics = None
        self.turbo = None
        self.collection = {}
        self.can_switch = True
        self.switching = False
        self.next_lane = None

    def update(self, data):
        old_position = self.position
        self.position = Position(data)
        self._travelled = roll(self._travelled, -1)
        self._travelled[2] = self._travelled[1] + self.track.distance_between(old_position, self.position, self.next_lane, int(self.switching))
        self.travelled = self._travelled[2]
        self.speed = dot([0, -1, 1], self._travelled)
        self.acc = dot([1, -2, 1], self._travelled)
        self._angles = roll(self._angles, -1)
        self._angles[2] = self.position.angle
        self.angle_speed = dot([0, -1, 1], self._angles)
        self.angle_acc = dot([1, -2, 1], self._angles)
        if old_position.piece != self.position.piece and self.track.pieces[old_position.piece].switch:
            self.can_switch = True
            self.switching = False
            self.next_lane = None

    def on_turbo_available(self, turbo_data):
        self.turbo = Turbo(turbo_data)

    def has_turbo(self):
        return self.turbo is not None
        
    def end_turbo(self):
        self.physics.disable_turbo()
        self.turbo = None

    def start_turbo(self):
        if self.turbo is not None:
            self.physics.enable_turbo(self.turbo.factor)

    def init_track(self, data):
        self.track = Track(data)
        self.physics = physics.car.Physics(0.02, 0.2)

    def init_geometry(self, data):
        dimensions = data['dimensions']
        front_to_center = dimensions['length'] / 2.0
        self.r_a = front_to_center - dimensions['guideFlagPosition']

    def is_curving(self):
        return self.track.pieces[self.position.piece].curve

    def is_next_late(self, target_speed):
        v_2 = self.physics.next_v(self.speed, 1)
        x_2 = self.physics.next_x(0, v_2, 1)
        distance_2 = self.track.distance_to_curve(self.position, self.next_lane, int(self.switching)) + x_2
        distance_to_speed_2 = self.physics.distance_to_speed(v_2, target_speed)
        return distance_2 < distance_to_speed_2

    def distance_to_late(self, target_speed):
        distance_1 = self.track.distance_to_curve(self.position, self.next_lane, int(self.switching))
        v_2 = self.physics.next_v(self.speed, 1)
        distance_to_speed_2 = self.physics.distance_to_speed(v_2, target_speed)
        return distance_1 - distance_to_speed_2

    def a_needed(self, tick_distance):
        return tick_distance - self.speed

    def throttle_needed(self, tick_distance):
        a = self.a_needed(tick_distance)
        t = self.physics.throttle(self.speed, a)
        if(t < 0):
            return 0
        if(t > 1):
            return 1
        else:
            return t

    def collect(self, key, value):
        if(key in self.collection):
            self.collection[key].append(value)
        else:
            self.collection[key] = [value]

    def dump(self):
        for key, value in self.collection.iteritems():
            self.print_data(key, value)

    def print_data(self, name, data):
        if not os.path.exists('data'):
            os.makedirs('data')
        with open('data/' + name, 'w') as myFile:
             for d in data:
                 myFile.write(str(d))
                 myFile.write("\n")

    def curvemeter(self):
        r_c = self.track.pieces[self.position.piece].r
        b = self.position.angle 
        r_m = physics.car.mass_center_radius(r_c, self.r_a, b)
        c = physics.car.centrifugal_angle(r_c, self.r_a, r_m)
        a = (self.speed ** 2 * math.sin(c)) / (self.r_a * r_c)
        F_c = physics.car.centrifugal_force(self.speed, r_c)
        F_v = physics.car.centrifugal_counter_force(r_c, self.angle_acc, c, F_c)
        F_1 = F_c - self.angle_speed
        if(self.angle_speed != 0):
            k = (F_c - self.angle_acc) / self.angle_speed
        else:
            k = 0
        self.collect('F_1', F_1)
        self.collect('k', k)
        self.collect('F_c', F_c)
        self.collect('F_v', F_v)
        self.collect('angle_acc', self.angle_acc)
        self.collect('b', b)
        self.collect('r_c', r_c)
        self.collect('r_m', r_m)
        self.collect('v', self.speed)
        self.collect('w', self.angle_speed)
        self.collect('power', self.physics.T)
        if(self.track.pieces[self.position.piece].a > 0):
            sign = 1
        else:
            sign = -1
        self.collect('sign', sign)

    def previous_angle(self):
        return self._angles[1]

    def game_state(self, tick):
        return GameState(tick, self.position, self.travelled, self.speed,
                         self.acc, self.angle_speed, self.angle_acc)

    def reset_state(self):
        self.speed = 0
        self.acc = 0
        self.angle_speed = 0
        self._angles = zeros(3)
        self.angle_speed = 0
        self.angle_acc = 0
        self.end_turbo()
