from numpy import zeros

class GameState:
    def __init__(self, tick, position, travelled, speed, acc, angle_speed, angle_acc):
        self.tick = tick
        self.position = position
        self.travelled = travelled
        self.speed = speed
        self.acc = acc
        self.angle_speed = angle_speed
        self.angle_acc = angle_acc

    def update(self, next_acc, next_angle_acc, track):
        next_pos = self.position.next(self.speed, self.angle_speed, track)
        next_travelled = self.travelled + self.speed
        next_speed = max(self.speed + self.acc, 0.0)
        next_angle_speed = self.angle_speed + self.angle_acc
        return GameState(self.tick+1, next_pos, next_travelled, next_speed,
                         next_acc, next_angle_speed, next_angle_acc)

    def angle(self):
        return self.position.angle

    def __str__(self):
        return str({'tick': self.tick, 'travelled': self.travelled,
                    'speed': self.speed, 'acc': self.acc,
                    'angle': self.angle(), 'angle_speed': self.angle_speed,
                    'angle_acc': self.angle_acc})
