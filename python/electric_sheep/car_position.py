class CarPosition:
    def __init__(self, piece=0, distance=0, lane=0):
        self.piece = piece
        self.distance = distance
        self.lane = lane

    def update(self, piece, distance, lane):
        self.piece = piece
        self.distance = distance
        self.lane = lane

    # Car is ahead the position
    def is_ahead(self, position):
        print self.piece, position.piece
        return self.piece > position.piece or (self.piece == position.piece and self.distance > position.distance)
